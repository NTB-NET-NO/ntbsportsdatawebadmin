﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;



namespace NTBSportsDataWeb.Controllers
{
    public class jobsController : Controller
    {
        //
        // GET: /jobs/

        JobsModels jobs = new JobsModels();

        

        

        public ActionResult Index()
        {
            // ViewBag.CustomerSteps = new SelectList(db.CustomerActions.ToList(), "CustomerActionId", "CustomerAction");

            @ViewBag.Kretser = new SelectList(jobs.getDistricts(), "DistrictId", "DistrictName");


            // @ViewBag.KommuneList = new SelectList(jobs.getMunicipalities(), "MunicipialityId", "MunicipalityName");
            return View();
        }

        //
        // GET: /jobs/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /jobs/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /jobs/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /jobs/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /jobs/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /jobs/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /jobs/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
