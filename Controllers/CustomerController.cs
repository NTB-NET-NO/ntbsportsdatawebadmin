﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;
using System.IO;

namespace NTBSportsDataWeb.Controllers
{
    public class CustomerController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();



        //
        // GET: /Customer/

        public ViewResult Index()
        {
            return View(db.Customers.ToList());
        }

        //
        // GET: /Customer/Details/5

        public ViewResult Details(int id)
        {
            Customer customer = db.Customers.Find(id);
            return View(customer);
        }

        //
        // GET: /Customer/Create

        public ActionResult Create()
        {
            /*string[] CustomerSteps = new string[] {"Kundeinfo", "Jobber", "Profil"};
            ViewBag.CustomerSteps = new SelectList(CustomerSteps);
             */
            ViewBag.CustomerSteps = new SelectList(db.CustomerActions.ToList(), "CustomerActionId", "CustomerAction");

            ViewBag.CustomerID = new SelectList(db.Customers, "CustomerId", "CustomerName");
            return View();
        }

        private bool ViewExists(string name)
        {
            ViewEngineResult result = ViewEngines.Engines.FindView(ControllerContext, name, null);
            return (result.View != null);
        }

        public ActionResult Choose(int CustomerId, int CustomerSteps)
        {
            if (ModelState.IsValid)
            {
                

                ViewBag.CustomerSteps = new SelectList(db.CustomerActions.ToList(), "CustomerActionId", "CustomerAction");

                ViewBag.CustomerID = new SelectList(db.Customers, "CustomerId", "CustomerName");
                
                // Get customer name and such
                Customer customer = db.Customers.Find(CustomerId);
                ViewBag.CustomerName = customer.CustomerName;

                // Now find the name of a possible partialview
                Customer_Actions ca = db.CustomerActions.Find(CustomerSteps);
                
                // Creating the PartialViewName
                string PartialViewName = "_" + ca.CustomerAction.ToString();

                if (ViewExists(PartialViewName))
                {


                    ViewBag.PartialViewName = PartialViewName;
                }
                
                switch (CustomerSteps)
                {
                    case 1: // KundeInfo
                        // MessageFormat messageFormat = db.MessageFormat.Find(CustomerId);
                        ViewBag.MessageFormats = new SelectList(db.MessageFormats, "FormatId", "Format");
                        ViewBag.CharacterSets = new SelectList(db.CharacterSets, "CharacterSetId", "CharacterSet");


                        

                        
                        
                        
                        break;

                    case 2: // Jobber
                        JobsModels jobs = new JobsModels();
                        ViewBag.Kommuner = new SelectList(db.Municipalities, "MunicipalityId", "MunicipalityName");
                        ViewBag.Kretser = new SelectList(jobs.getDistricts(), "DistrictId", "DistrictName");

                        break;

                    case 3:

                        break;
                }



                // Customer customer = db.Customers.Find(id);

            }
            return View("Create");

        }

        //
        // POST: /Customer/Create

        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            return View(customer);
        }

        //
        // GET: /Customer/Edit/5

        public ActionResult Edit(int id)
        {
            Customer customer = db.Customers.Find(id);
            return View(customer);
        }

        //
        // POST: /Customer/Edit/5

        [HttpPost]
        public ActionResult Edit(Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        //
        // GET: /Customer/Delete/5

        public ActionResult Delete(int id)
        {
            Customer customer = db.Customers.Find(id);
            return View(customer);
        }

        //
        // POST: /Customer/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}