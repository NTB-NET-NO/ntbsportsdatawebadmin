﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

namespace NTBSportsDataWeb.Controllers
{ 
    public class SportsController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        //
        // GET: /Sports/

        public ViewResult Index()
        {
            return View(db.Sports.ToList());
        }

        //
        // GET: /Sports/Details/5

        public ViewResult Details(int id)
        {
            Sports sports = db.Sports.Find(id);
            return View(sports);
        }

        //
        // GET: /Sports/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Sports/Create

        [HttpPost]
        public ActionResult Create(Sports sports)
        {
            if (ModelState.IsValid)
            {
                db.Sports.Add(sports);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(sports);
        }
        
        //
        // GET: /Sports/Edit/5
 
        public ActionResult Edit(int id)
        {
            Sports sports = db.Sports.Find(id);
            return View(sports);
        }

        //
        // POST: /Sports/Edit/5

        [HttpPost]
        public ActionResult Edit(Sports sports)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sports).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sports);
        }

        //
        // GET: /Sports/Delete/5
 
        public ActionResult Delete(int id)
        {
            Sports sports = db.Sports.Find(id);
            return View(sports);
        }

        //
        // POST: /Sports/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Sports sports = db.Sports.Find(id);
            db.Sports.Remove(sports);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}