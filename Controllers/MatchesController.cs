﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

// using outside System libraries
using PagedList;

namespace NTBSportsDataWeb.Controllers
{ 
    public class MatchesController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();
        private MatchesModel matchesModel = new MatchesModel();
        //
        // GET: /Matches/

        


        
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
     
            ViewBag.CurrentSort = sortOrder;
            ViewBag.HomeTeamSortParm = sortOrder == "Hjemmelag" ? "Hjemmelag desc" : "Hjemmelag";
            ViewBag.AwayTeamSortParm = sortOrder == "Bortelag" ? "Bortelag desc" : "Bortelag";
            ViewBag.SportSortParm = sortOrder == "Sport" ? "Sport desc" : "Sport";
            ViewBag.DateSortParm = sortOrder == "Kampdato" ? "Kampdato desc" : "Kampdato";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }

            ViewBag.CurrentFilter = searchString;


            var matches = from m in db.Matches
                   where m.Date < DateTime.Now
                   select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                matches = matches.Where(s => s.HomeTeam.ToUpper().Contains(searchString.ToUpper())
                    || s.AwayTeam.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Bortelag desc":
                    matches = matches.OrderByDescending(m => m.AwayTeam);
                    break;

                case "Bortelag":
                    matches = matches.OrderBy(m => m.AwayTeam);
                    break;

                case "Hjemmelag desc":
                    matches = matches.OrderByDescending(m => m.HomeTeam);
                    break;

                case "Kampdato":
                    matches = matches.OrderBy(m => m.Date);
                    break;

                case "Kampdato desc":
                    matches = matches.OrderByDescending(m => m.Date);
                    break;

                case "Sport":
                    matches = matches.OrderBy(m => m.Sports);
                    break;

                case "Sport desc":
                    matches = matches.OrderByDescending(m => m.Sports);
                    break;

                default:
                    matches = matches.OrderBy(m => m.HomeTeam);
                    break;

            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);


            return View(matches.ToPagedList(pageNumber, pageSize));
        }
        

        //
        // GET: /Matches/Details/5

        public ViewResult Details(int id)
        {
            Matches matches = db.Matches.Find(id);
            return View(matches);
        }

        //
        // GET: /Matches/Create

        public ActionResult Create()
        {
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1");
            return View();
        } 

        //
        // POST: /Matches/Create

        [HttpPost]
        public ActionResult Create(Matches matches)
        {
            if (ModelState.IsValid)
            {
                db.Matches.Add(matches);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1", matches.SportId);
            return View(matches);
        }
        
        //
        // GET: /Matches/Edit/5
 
        public ActionResult Edit(int id)
        {
            Matches matches = db.Matches.Find(id);
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1", matches.SportId);
            return View(matches);
        }

        //
        // POST: /Matches/Edit/5

        [HttpPost]
        public ActionResult Edit(Matches matches)
        {
            if (ModelState.IsValid)
            {
                db.Entry(matches).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports", matches.SportId);
            return View(matches);
        }

        //
        // GET: /Matches/Delete/5
 
        public ActionResult Delete(int id)
        {
            Matches matches = db.Matches.Find(id);
            return View(matches);
        }

        //
        // POST: /Matches/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Matches matches = db.Matches.Find(id);
            db.Matches.Remove(matches);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}