﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace NTBSportsDataWeb.Models
{
    public class JobsModels
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        public IEnumerable<Tournaments>     Tournaments { get; set; }
        public IEnumerable<AgeCategory>     AgeCategory { get; set; }
        public IEnumerable<Sports>          Sports { get; set; }
        public IOrderedQueryable<Districts> Districts { get; set; }
        public IEnumerable<Municipality>    Municipality { get; set; }
        
        public IQueryable<Municipality> getMunicipalities(Districts district)
        {
            return from municipality in db.Municipalities
                   where municipality.DistrictId == district.DistrictId
                   orderby municipality.MunicipalityName
                   select municipality;
        }

        

        // Creating a model that does not use NFF but uses the database
        public IOrderedQueryable<Districts> getDistricts()
        {
            return from district in db.Districts
                   orderby district.DistrictId
                   select district;
        }
        
    }
}