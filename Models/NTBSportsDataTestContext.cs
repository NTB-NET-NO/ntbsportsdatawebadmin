﻿using System.Data.Entity;

namespace NTBSportsDataWeb.Models
{
    public class NTBSportsDataTestContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<NTBSportsDataWeb.Models.NTBSportsDataTestContext>());

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Districts> Districts { get; set; }

        public DbSet<Matches> Matches { get; set; }

        public DbSet<Sports> Sports { get; set; }

        public DbSet<Municipality> Municipalities { get; set; }

        public DbSet<Tournaments> Tournaments { get; set; }

        public DbSet<Seasons> Seasons { get; set; }

        public DbSet<Customer_Actions> CustomerActions { get; set; }

        public DbSet<MessageFormats> MessageFormats { get; set; }

        public DbSet<CharacterSets> CharacterSets { get; set; }

        public DbSet<JobsModels> JobsModels { get; set; }

    }
}
