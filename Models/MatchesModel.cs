﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTBSportsDataWeb.Models
{
    public class MatchesModel
    {
        // this class shall deal with anything related to matches
        // one of these are lists that shall be paginated. This because we get a .... 
        // of a lot of matches in the database

        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        public IQueryable<Matches> FindAllMatches()
        {
            return db.Matches;
        }

        public IQueryable<Matches> PaginateMatches()
        {
            return from matches in db.Matches
                   where matches.Date < DateTime.Now
                   orderby matches.Time
                   select matches;
            // return db.Matches;
        }


        /*
         * public IQueryable<Dinner> FindUpcomingDinners() {
        return from dinner in db.Dinners
               where dinner.EventDate > DateTime.Now
               orderby dinner.EventDate
               select dinner;
    }

    public Dinner GetDinner(int id) {
        return db.Dinners.SingleOrDefault(d => d.DinnerID == id);
    }

    //
    // Insert/Delete Methods

    public void Add(Dinner dinner) {
        db.Dinners.InsertOnSubmit(dinner);
    }

    public void Delete(Dinner dinner) {
        db.RSVPs.DeleteAllOnSubmit(dinner.RSVPs);
        db.Dinners.DeleteOnSubmit(dinner);
    }

    //
    // Persistence 

    public void Save() {
        db.SubmitChanges();
    }
         */
    }

}
