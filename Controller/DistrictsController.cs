﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

namespace NTBSportsDataWeb.Controllers
{ 
    public class DistrictsController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        //
        // GET: /Districts/

        public ViewResult Index()
        {
            return View(db.Districts.ToList());
        }

        //
        // GET: /Districts/Details/5

        public ViewResult Details(int id)
        {
            Districts districts = db.Districts.Find(id);
            return View(districts);
        }

        //
        // GET: /Districts/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Districts/Create

        [HttpPost]
        public ActionResult Create(Districts districts)
        {
            if (ModelState.IsValid)
            {
                db.Districts.Add(districts);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(districts);
        }
        
        //
        // GET: /Districts/Edit/5
 
        public ActionResult Edit(int id)
        {
            Districts districts = db.Districts.Find(id);
            return View(districts);
        }

        //
        // POST: /Districts/Edit/5

        [HttpPost]
        public ActionResult Edit(Districts districts)
        {
            if (ModelState.IsValid)
            {
                db.Entry(districts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(districts);
        }

        //
        // GET: /Districts/Delete/5
 
        public ActionResult Delete(int id)
        {
            Districts districts = db.Districts.Find(id);
            return View(districts);
        }

        //
        // POST: /Districts/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Districts districts = db.Districts.Find(id);
            db.Districts.Remove(districts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}