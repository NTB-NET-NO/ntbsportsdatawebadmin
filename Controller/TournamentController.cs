﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

namespace NTBSportsDataWeb.Controllers
{ 
    public class TournamentController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        //
        // GET: /Tournament/

        public ViewResult Index()
        {
            var tournaments = db.Tournaments.Include(t => t.Districts).Include(t => t.Seasons).Include(t => t.Sports);
            return View(tournaments.ToList());
        }

        //
        // GET: /Tournament/Details/5

        public ViewResult Details(int id)
        {
            Tournaments tournaments = db.Tournaments.Find(id);
            return View(tournaments);
        }

        //
        // GET: /Tournament/Create

        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName");
            ViewBag.SeasonId = new SelectList(db.Seasons, "SeasonId", "SeasonName");
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1");
            return View();
        } 

        //
        // POST: /Tournament/Create

        [HttpPost]
        public ActionResult Create(Tournaments tournaments)
        {
            if (ModelState.IsValid)
            {
                db.Tournaments.Add(tournaments);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName", tournaments.DistrictId);
            ViewBag.SeasonId = new SelectList(db.Seasons, "SeasonId", "SeasonName", tournaments.SeasonId);
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1", tournaments.SportId);
            return View(tournaments);
        }
        
        //
        // GET: /Tournament/Edit/5
 
        public ActionResult Edit(int id)
        {
            Tournaments tournaments = db.Tournaments.Find(id);
            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName", tournaments.DistrictId);
            ViewBag.SeasonId = new SelectList(db.Seasons, "SeasonId", "SeasonName", tournaments.SeasonId);
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1", tournaments.SportId);
            return View(tournaments);
        }

        //
        // POST: /Tournament/Edit/5

        [HttpPost]
        public ActionResult Edit(Tournaments tournaments)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tournaments).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName", tournaments.DistrictId);
            ViewBag.SeasonId = new SelectList(db.Seasons, "SeasonId", "SeasonName", tournaments.SeasonId);
            ViewBag.SportId = new SelectList(db.Sports, "SportId", "Sports1", tournaments.SportId);
            return View(tournaments);
        }

        //
        // GET: /Tournament/Delete/5
 
        public ActionResult Delete(int id)
        {
            Tournaments tournaments = db.Tournaments.Find(id);
            return View(tournaments);
        }

        //
        // POST: /Tournament/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Tournaments tournaments = db.Tournaments.Find(id);
            db.Tournaments.Remove(tournaments);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}