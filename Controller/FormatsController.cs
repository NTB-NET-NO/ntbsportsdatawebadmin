﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

namespace NTBSportsDataWeb.Controllers
{ 
    public class FormatsController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        //
        // GET: /Formats/

        public ViewResult Index()
        {
            return View(db.MessageFormats.ToList());
        }

        //
        // GET: /Formats/Details/5

        public ViewResult Details(int id)
        {
            MessageFormats messageformats = db.MessageFormats.Find(id);
            return View(messageformats);
        }

        //
        // GET: /Formats/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Formats/Create

        [HttpPost]
        public ActionResult Create(MessageFormats messageformats)
        {
            if (ModelState.IsValid)
            {
                db.MessageFormats.Add(messageformats);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(messageformats);
        }
        
        //
        // GET: /Formats/Edit/5
 
        public ActionResult Edit(int id)
        {
            MessageFormats messageformats = db.MessageFormats.Find(id);
            return View(messageformats);
        }

        //
        // POST: /Formats/Edit/5

        [HttpPost]
        public ActionResult Edit(MessageFormats messageformats)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messageformats).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messageformats);
        }

        //
        // GET: /Formats/Delete/5
 
        public ActionResult Delete(int id)
        {
            MessageFormats messageformats = db.MessageFormats.Find(id);
            return View(messageformats);
        }

        //
        // POST: /Formats/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            MessageFormats messageformats = db.MessageFormats.Find(id);
            db.MessageFormats.Remove(messageformats);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}