﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

namespace NTBSportsDataWeb.Controllers
{ 
    public class MunicipalityController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        //
        // GET: /Municipality/

        public ViewResult Index()
        {
            var municipality = db.Municipalities.Include(m => m.Districts);
            return View(municipality.ToList());
        }

        //
        // GET: /Municipality/Details/5

        public ViewResult Details(int id)
        {
            Municipality municipality = db.Municipalities.Find(id);
            return View(municipality);
        }

        //
        // GET: /Municipality/Create

        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName");
            return View();
        } 

        //
        // POST: /Municipality/Create

        [HttpPost]
        public ActionResult Create(Municipality municipality)
        {
            if (ModelState.IsValid)
            {
                db.Municipalities.Add(municipality);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName", municipality.DistrictId);
            return View(municipality);
        }
        
        //
        // GET: /Municipality/Edit/5
 
        public ActionResult Edit(int id)
        {
            Municipality municipality = db.Municipalities.Find(id);
            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName", municipality.DistrictId);
            return View(municipality);
        }

        //
        // POST: /Municipality/Edit/5

        [HttpPost]
        public ActionResult Edit(Municipality municipality)
        {
            if (ModelState.IsValid)
            {
                db.Entry(municipality).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DistrictId = new SelectList(db.Districts, "DistrictId", "DistrictName", municipality.DistrictId);
            return View(municipality);
        }

        //
        // GET: /Municipality/Delete/5
 
        public ActionResult Delete(int id)
        {
            Municipality municipality = db.Municipalities.Find(id);
            return View(municipality);
        }

        //
        // POST: /Municipality/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Municipality municipality = db.Municipalities.Find(id);
            db.Municipalities.Remove(municipality);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}