﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTBSportsDataWeb.Models;

namespace NTBSportsDataWeb.Controllers
{ 
    public class CharacterSetsController : Controller
    {
        private NTBSportsDataTestContext db = new NTBSportsDataTestContext();

        //
        // GET: /CharacterSets/

        public ViewResult Index()
        {
            return View(db.CharacterSets.ToList());
        }

        //
        // GET: /CharacterSets/Details/5

        public ViewResult Details(int id)
        {
            CharacterSets charactersets = db.CharacterSets.Find(id);
            return View(charactersets);
        }

        //
        // GET: /CharacterSets/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /CharacterSets/Create

        [HttpPost]
        public ActionResult Create(CharacterSets charactersets)
        {
            if (ModelState.IsValid)
            {
                db.CharacterSets.Add(charactersets);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(charactersets);
        }
        
        //
        // GET: /CharacterSets/Edit/5
 
        public ActionResult Edit(int id)
        {
            CharacterSets charactersets = db.CharacterSets.Find(id);
            return View(charactersets);
        }

        //
        // POST: /CharacterSets/Edit/5

        [HttpPost]
        public ActionResult Edit(CharacterSets charactersets)
        {
            if (ModelState.IsValid)
            {
                db.Entry(charactersets).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(charactersets);
        }

        //
        // GET: /CharacterSets/Delete/5
 
        public ActionResult Delete(int id)
        {
            CharacterSets charactersets = db.CharacterSets.Find(id);
            return View(charactersets);
        }

        //
        // POST: /CharacterSets/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            CharacterSets charactersets = db.CharacterSets.Find(id);
            db.CharacterSets.Remove(charactersets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}